# UEV-X

Búsqueda de un reemplazo para el DistoX.

El SAP parece ser una buena base.
Sería razonable adquirir uno (£280.00), probarlo tal cual, y estudiar cómo mejorarlo y/o ajustar el coste de fabricación
para varios grupos.
Si la calidad de la IMU integrada es suficiente, la elección de componentes es muy interesante, y en tiradas de 5-10 el
coste podría reducirse a 225-250€ por unidad, que es menos de lo que costaba/cuesta el Disto x310, y la mitad de lo que
cuesta un DistoX completo (500€).

Posibles mejoras:

- Carcasa
- Pantalla
- Botones
- Disposición de los componentes
  - Distancia entre el sensor de distancia y el/los magnetómetro(s)
- Inspirándonos en el BRIC, duplicar el magnetómetro y la IMU.
- Batería no magnética

En cuanto a la calidad del firmware, si en algún momento el uso de Python supusiera una limitación, el mismo hardware
podría programarse en C (o Rust).
